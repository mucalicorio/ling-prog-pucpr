def cria_matriz(quantidade_linhas, quantidade_colunas, elemento=0):
    matriz = []
    for linha in range(quantidade_linhas):
        construtor_coluna = []
        for coluna in range(quantidade_colunas):
            construtor_coluna.append(elemento)

        matriz.append(construtor_coluna)

    return matriz


quantidade_linhas = int(input("Quantidade de linhas: "))
quantidade_colunas = int(input("Quantidade de colunas: "))
elemento = input("Elemento: ")

matriz = cria_matriz(quantidade_linhas, quantidade_colunas, elemento)

for linha in matriz:
    for coluna in linha:
        print(coluna, end=" ")
    print()
