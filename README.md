### Checklist de atividades

- [x] Lab 01
    - [x] 01 IMC
    - [x] 02 Casas decimais
    - [x] 03 Papelaria
    - [x] **04 Estacionamento**
    - [x] 05 IRRF
    - [x] 06 Joquempô
- [ ] Lab 02
    - [ ] 01 Crecimento Anacleto e Felisberto
    - [x] 02 While sequência número
    - [x] **03 Notas dos juízes**
    - [x] 04 Valor de π
    - [ ] 05 Array de 1 a 100 aleatório
    - [x] 06 Preenche valor aleatório
    - [x] **07 Ordenação por seleção direta**
    - [x] **08 Impressão do número por extenso**
    - [x] 09 Jogo forca
- [ ] Lab 03
    - [ ] **01 CPF**

    Obs: os exercícios em **negrito** são os entregáveis para avaliação.
