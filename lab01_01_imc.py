# [1] O IMC, índice de massa corporal, é calculado através da seguinte fórmula:
# IMC = massa / altura2
# Elabore um programa que leia a massa(em quilogramas)
# e a altura(em metros) do usuário e mostre o valor do IMC.

massa = float(input("Digite a massa: "))
altura = float(input("Digite a altura: "))

imc = massa / (altura ** 2)

print("O IMC é igual a: ", imc)
