# [2] Elabore um programa que leia um número inteiro
# e considerando que este possui três casa decimais,
# mostre o valor da centena, da dezena e da unidade.

numero = int(input("Digite um número inteiro: "))

centena = numero // 100
numero = numero % 100
print("Centena: ", centena)

dezena = numero // 10
print("Decimal: ", dezena)

unidade = numero % 10
print("Unidade: ", unidade)
