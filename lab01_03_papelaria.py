# [3] Em uma determinada papelaria a fotocópia custa R$ 0,25,
# caso sejam tiradas menos de 100 cópias. A partir de 100 cópias,
# o valor de cada fotocópia tirada cai para R$ 0,20. Elabore um
# programa que leia o número de cópias e mostre o valor a pagar
# pelo serviço.

quantidade = int(input("Digite a quantidade de fotos: "))

if (quantidade < 100):
    valor_total = quantidade * 0.25
else:
    valor_total = quantidade * 0.2

print(str(quantidade) + " fotos ficam: R$" + str(valor_total))
