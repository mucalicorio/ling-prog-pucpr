# [4] Em um determinado estacionamento a primeira hora
# custa R$ 8,00, que é o valor mínimo praticado. Após
# uma hora o valor é fracionado, R$ 1,50 a cada 15 minutos.
# Elabore um programa que leia um número inteiro correspondente
# a quantidade de minutos usados no estacionamento e mostre a
# mensagem “Valor mínimo, R$ 8,00” ou “Valor fracionado, R$ x”,
# no qual x será o valor a pagar calculado pelo algoritmo.

a_pagar = float(8)

tempo = float(input("Digite o tempo em minutos: "))

if (tempo >= 60):
    tempo = tempo - 60
    while tempo > 0:
        tempo = tempo - 15
        a_pagar += 1.5

print("Preço a pagar: R$", a_pagar)
