# [5] A tabela progressiva mensal do IRRF (Imposto de Renda
# Retido na Fonte) estabelece as seguintes alíquotas (para
# o ano-calendário de 2018):
#  ______________________________ ____________
# |-Base de cálculo mensal em R$-|-
# |------------------------------|-
# |-Até 1.903,98-----------------|-
# |------------------------------|-
# |-De 1.903,99 até 2.826,65-----|-
# |------------------------------|-
# |-De 2.826,66 até 3.751,05-----|-
# |------------------------------|-
# |-De 3.751,06 até 4.664,68-----|-
# |------------------------------|-
# |-Acima de 4.664,69------------|-
# |______________________________|____________
# Considerando que o valor da dedução mensal por dependente é
# de R$ 189,59; escreva um programa que leia o salário de um
# funcionário (rendimento tributável mensal) e calcule o valor
# do imposto devido. Para verificar a estratégia de cálculo e
# comparar seus resultados use o simulador disponível no site
# da Receita Federal em:
# http://www.receita.fazenda.gov.br/Aplicacoes/ATRJO/Simulador/simulador.asp

salario = float(input("Salário: "))

impostos = 0

if (salario <= 1903.98):
    impostos = (salario * 0) / 100
elif (salario <= 2826.65):
    impostos = (salario * 7.5) / 100
elif (salario <= 3751.05):
    impostos = (salario * 15) / 100
elif (salario <= 4664.68):
    impostos = (salario * 22.5) / 100
else:
    impostos = (salario * 27.5) / 100

if (impostos > 0):
    dependentes = int(input("Dependentes: "))
    impostos = impostos - (dependentes * 189.59)

print("Impostos: ", impostos)
