# [6] Escreva um algoritmo que jogue Joquempô (Pedra, Papel e
# Tesoura) com o usuário; ou seja ele escolhe randomicamente
# sua jogada, lê a opção do usuário e mostra o resultado:
# vitória do computador, vitória do usuário ou empate.

from random import randint


print("0 - Pedra")
print("1 - Papel")
print("2 - Tesoura")
humano = int(input(""))

computador = randint(0, 2)

if (humano == computador):
    resultado = "EMPATE"
elif (humano == 0 and computador == 1):
    resultado = "COMPUTADOR"
elif (humano == 0 and computador == 2):
    resultado = "HUMANO"
elif (humano == 1 and computador == 0):
    resultado = "HUMANO"
elif (humano == 1 and computador == 2):
    resultado = "COMPUTADOR"
elif (humano == 2 and computador == 0):
    resultado = "COMPUTADOR"
elif (humano == 2 and computador == 1):
    resultado = "HUMANO"

print("Humano: ", humano)
print("Computador: ", computador)
print("------------------------")
print("Resultado: ", resultado)
