# [2] Escreva um programa que leia um conjunto de números inteiros
# e que somente termine a leitura quando for fornecido como
# finalizador o mesmo valor fornecido no início da sequência.
# Informe então qual é a média aritmética do conjunto, excluindo
# o primeiro e o último.

lista = []
numero = int(input("Adicione um número: "))

lista.append(numero)
numero_parada = numero
soma = 0
numero = None

while numero != numero_parada:
    numero = int(input("Adicione um número: "))
    lista.append(numero)
    soma += numero

lista.pop()
lista.pop(0)

media = (soma) / len(lista)
print(lista)
print("Média: ", media)
