# [3] Em uma determinada competição esportiva cada
# participante recebe as notas de seis juízes. A
# melhor e a pior nota são eliminadas, sendo a nota
# do participante a média das outras quatro notas.
# Elabore um algoritmo que leia as seis notas de um
# atleta e depois informe qual foi seu resultado final.

notas = []

votos_restantes = 6

print("Os votos podem ser inteiros (ex: 7) ou com ponto flutuante (ex: 6.8).\n")

while votos_restantes > 0:
    notas.append(float(input("Voto do juiz: ")))
    votos_restantes -= 1

notas.remove(min(notas))
notas.remove(max(notas))

media_notas = sum(notas)/len(notas)

print("Média das notas:", media_notas)
