# [4] O valor de π pode ser calculado usando como base a seguinte série:
# S = 1 - 1/3³ + 1/5³ - 1/7³ + 1/9³ - ... +- ?
# Sendo,
# π = 3\/S*32
# Elabore um programa que calcule e mostre o valor de π com base
# na quantidade de termos solicitada pelo usuário.

soma = 0
elevado_a = 1
contador = 0
parada = int(input("Número de repetições: "))

while contador < parada:
    if ((contador % 2) != 1):
        soma = soma + ((1 / elevado_a) ** 3)
    else:
        soma = soma - ((1 / elevado_a) ** 3)

    elevado_a += 2
    contador += 1

valor_pi = (soma * 32) ** (1/3)
print("Valor estimado de π: ", valor_pi)
