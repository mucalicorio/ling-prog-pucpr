# [6] Escreva um programa que preencha um vetor
# de 10 posições com valores inteiros aleatórios
# entre [0,5] . Depois, utilize outro vetor para
# calcular a distribuição de frequência das
# ocorrências dos valores possíveis para o vetor.
# Mostre então a distribuição calculada.

from random import randint

contador = 0
lista = []

while contador < 10:
    lista.append(randint(0, 5))
    contador += 1

print("Lista:", lista)
dicionario = {}

for item in lista:
    dicionario[item] = 0
    for outro_item in lista:
        if item == outro_item:
            dicionario[item] += 1

print(dicionario)
