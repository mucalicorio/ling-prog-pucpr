# [7] Desenvolva um programa que preencha um vetor
# de 20 posições com números inteiros aleatórios entre
# [10, 99], e o coloque em ordem crescente, utilizando
# a seguinte estratégia de ordenação (Seleção Direta):
#     • selecione o elemento do vetor de 20 posições que
# apresenta o menor valor;
#     • troque este elemento pelo primeiro;
#     • repita estas operações, envolvendo agora apenas os
# 19 elementos restantes (trocando o de menor valor com
# a segunda posição), depois os 18 elementos (trocando
# o de menor valor com a terceira posição), depois os 17,
# 16 e assim por diante, até restar um único elemento,
# o maior deles

from random import randint

lista = []
while (len(lista) < 20):
    lista.append(randint(10, 99))

print(lista)

for index_list in range(len(lista)):
    nova_posicao = (lista[index_list:].index(min(lista[index_list:]))) + index_list
    lista[index_list], lista[nova_posicao] = lista[nova_posicao], lista[index_list]

print(lista)
