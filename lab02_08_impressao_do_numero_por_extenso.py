# [8] Escreva um programa que leira um número inteiro,
# caso ele esteja dentro do intervalo de 1 a 999, mostre
# seu valor por extenso, caso contrário mostre "valor forne-
# cido fora dos limites operacionais". Ou seja, se o usuário
# digitar 158, o programa mostra "158: cento e cinquenta e oito".
# Utilize vetor de tring para representar as unidades (1..9), dezenas
# especiais (10, 20 etc) e centenas (100, 200 etc). Separe, usando
# mod e div, o valor da unidade, da dezena e da centena do número
# fornecido e use estes valores como índices de consulta aos vetores
# contendo as strings equivalentes. Serão necessários vários testes
# para verificar o uso da letra "e" entre os valores. Para testar sua
# implementação, faça uma repetição de 1 a 999, e veja se todos estão
# grafados corretamentes.

unidades = ["zero", "um", "dois", "três", "quatro",
            "cinco", "seis", "sete", "oito", "nove"]

especiais = ["dez", "onze", "doze", "treze", "quatorze",
             "quinze", "dezesseis", "dezessete", "dezoito", "dezenove"]

dezenas = ["vinte", "trinta", "quarenta", "cinquenta",
           "sessenta", "setenta", "oitenta", "noventa"]

centenas = ["cento", "duzentos", "trezentos", "quatrocentos",
            "quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos"]


def centena(numero, string=""):
    centena = int(numero/100)
    if(centena == 1 and numero % 100 == 0):
        return "cem"
    if(centena > 0):
        string += str(centenas[centena-1])
        if(numero % 100 == 0):
            return string
        string += " e "
    return dezena(numero % 100, string)


def dezena(numero, string):
    dezena = int(numero/10)
    if(dezena > 1):
        string += str(dezenas[dezena-2])
        if(numero % 10 == 0):
            return string
        string += " e "
    elif(dezena == 1):
        string += str(especiais[numero % 10])
        return string
    return string + str(unidades[numero % 10])


def init(numero):
    return "número com mais de três digitos" if numero >= 1000 else str(unidades[numero]) if numero == 0 else centena(numero)

numero_input = int(input("\n\nDigite um número de até três dígitos -> "))

print(init(numero_input))

# for number in range(0, 1000):
#     print(init(number), end=", ", flush=True)
