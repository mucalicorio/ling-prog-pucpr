# [9] Desenvolva um programa para o “Jogo da Forca”. Para tal,
# crie um vetor de strings inicializado com um conjunto de
# palavras-chave (por exemplo: nomes de capitais do Brasil,
# ou times de futebol da Serie A ou Países da América do Sul,
# etc). Sorteie uma das palavras para ser o segredo e forneça
# seis vidas para o usuário acertar o segredo. A cada rodada
# informe o número de vidas disponíveis e a disposição das
# letras acertadas e ausentes na palavra segredo (lembre de
# quando brincava com este jogo em caderno na infância), mostre
# também quais as letras que já foram.

from random import randint

palavras = ["Sao Paulo", "Flamengo", "Avai", "Goias",
            "Chapecoense", "Palmeiras", "Vasco da Gama", "Cruzeiro"]
segredo = (palavras[randint(0, len(palavras))]).upper()

mostra_inicial = []
for letra in segredo:
    if letra == " ":
        mostra_inicial.append(" ")
    else:
        mostra_inicial.append("_")

for mostrar in mostra_inicial:
    print(mostrar, end=" ")

lista_restantes = []
for letra in segredo:
    if letra != " ":
        lista_restantes.append(letra.upper())

vidas = 6
acertadas = []
continua = True
while ((vidas > 0) and continua):
    print("\nVidas:", vidas)
    jogada = input("Digite uma letra: ")

    acertou = False
    for letra in segredo.replace(' ', ''):
        if jogada.upper() == letra.upper():
            acertadas.append(letra.upper())
            try:
                lista_restantes.remove(letra)
            except:
                pass
            acertou = True

    mostra = []

    for letra in segredo:
        if letra == " ":
            mostra.append(" ")
        elif letra in acertadas:
            mostra.append(letra)
        elif letra not in acertadas:
            mostra.append("_")

    for mostrar in mostra:
        print(mostrar, end=" ")
    print()

    if lista_restantes == []:
        continua = False
    elif acertou == False:
        vidas -= 1

    print("\n" * 5)
    print("*" * 50)
    print("\n" * 5)

print("\n" * 5)
print("!!" * 100)
print("\n" * 5)
if continua == False:
    print("PARABÉNS, VOCê VENCEU!")
else:
    print("PERDEU")
print("\n" * 5)
print("!!" * 100)
print("\n" * 5)
