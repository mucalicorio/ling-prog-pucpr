# Membros:
# 1 - Lucas Fernando dos Santos
# 2 - Marcio Manoel Polonio Machado
# 3 - Michel de Souza Pinto
# 4 - Samuel Licorio Leiva


class CPF(object):
    def validaCpf(self, cpf, digito1=0, digito2=0, contador=0):
        while contador < 10:
            digito1, digito2, contador = (
                digito1 + (int(cpf[contador]) * (11 - contador - 1))) % 11 if contador < 9 else digito1, (digito2 + (int(cpf[contador]) * (11 - contador))) % 11, contador + 1
        return (int(cpf[9]) == (11-digito1 if digito1 > 1 else 0)) and (int(cpf[10]) == (11 - digito2 if digito2 > 1 else 0))


cpf_a_validar = input("\nDigite o cpf a validar: ")

cpf_a_validar = cpf_a_validar.replace(".", "").replace("-", "")

if (CPF().validaCpf(cpf_a_validar)):
    print("\n----CPF Válido----\n")
else:
    print("\n----CPF Inválido----\n")
