import java.util.Calendar;

public class Despesa {
    private final static String[] cat = { "Alimentacao", "Lazer", "Conta Fixa" };
    private float valor;
    private String descricao;
    private int categoria;
    private Calendar data;

    // construtor permitindo atribui valor, descricao e categoria da despesa
    public Despesa(float valor, String descricao, int categoria) {
        super();
        this.valor = valor;
        this.descricao = descricao;
        this.categoria = categoria;
        this.data = Calendar.getInstance();
        // data corrente alocada por default!
    }

    // sobrecarga do construtor, permitindo categoria default ser alimentacao
    public Despesa(float valor, String descricao) {
        this(valor, descricao, 0);
    }

    public float getValor() {
        return valor;
    }

    public String getDescricao() {
        return descricao;
    }

    public String getCategoriaDesc(int i) {
        return cat[i];
    }

    public int getCategoria() {
        return categoria;
    }

    public String getDataDesc() {
        return data.get(Calendar.DAY_OF_MONTH) + "/" + (data.get(Calendar.MONTH) + 1) + "/" + data.get(Calendar.YEAR);
    }

}
