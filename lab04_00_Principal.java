import java.util.ArrayList;
 
public class Principal {
 
    public static void main(String[] args) {
        ArrayList<Despesa> despesas = new ArrayList<Despesa>();
        despesas.add(new Despesa(14.80F, "Almoco cantina universidade"));
        despesas.add(new Despesa(120.25F, "Telefone Fixo", 2));
        despesas.add(new Despesa(60.00F, "Cinema: Rei Leao", 1));
        despesas.add(new Despesa(49.80F, "Lanche praca de alimentacao"));
         
        float soma = 0;
        for (int i = 0; i < despesas.size(); i++) {
            Despesa d = despesas.get(i);
            System.out.println (i + " -  " + d.getDescricao());
            System.out.println ("Valor: " + d.getValor());
            System.out.println ("Categoria: " + d.getCategoriaDesc(d.getCategoria()));
            System.out.println ("Data: " + d.getDataDesc());
            soma += d.getValor();
            System.out.println();
        }
        System.out.println("Total dos gastos: R$ " + soma);
    }
}