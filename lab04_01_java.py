from datetime import datetime


class Despesa():
    def getValor(self):
        return self.valor

    def getDescricao(self):
        return self.descricao

    def getCategoriaDesc(self, i):
        return self.cat[i]

    def getCategoria(self):
        return self.categoria

    def getDataDesc(self):
        return self.data.strftime("%d/%m/%Y")

    def __init__(self, valor, descricao, categoria=0):
        self.valor = valor
        self.descricao = descricao
        self.categoria = categoria
        self.data = datetime.today()
        self.cat = ["Alimentação", "Lazer", "Conta Fixa"]


despesas = []
despesas.append(Despesa(14.8, "Almoco cantina universidade"))
despesas.append(Despesa(120.25, "Telefone Fixo", 2))
despesas.append(Despesa(60, "Cinema: Rei Leão", 1))
despesas.append(Despesa(49.8, "Lanche praca de alimentacao"))

soma = 0

for index, despesa in enumerate(despesas):
    print(f"{index + 1} - {despesa.getDescricao()}")
    print(f"Valor: {despesa.getValor()}")
    print(f"Categoria: {despesa.getCategoriaDesc(despesa.getCategoria())}")
    print(f"Data: {despesa.getDataDesc()}")
    soma += despesa.getValor()
    print()

print(f"Total dos gastos: R$ {soma}")
