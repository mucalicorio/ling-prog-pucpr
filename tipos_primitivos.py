# Entrada de dados
print("--------ENTRADA DE DADOS--------")

numero1 = input("Digite o número 1: ")
print("Tipo da variável numero1: ", type(numero1))

numero2 = input("Digite o número 2: ")
print("Tipo da variável numero2: ", type(numero2))

# Processamento
print("\n--------PROCESSAMENTO--------")

numero1 = int(numero1)
print("Tipo da variável numero1: ", type(numero1))

numero2 = int(numero2)
print("Tipo da variável numero2: ", type(numero2))
media = (numero1 + numero2) / 2
print("Tipo da variável media: ", type(media))

# Saída de dados
print("\n--------SAÍDA DE DADOS--------")
print("A media de", numero1, "e", numero2, "é: ", media)
